package ${packagePath}.dal.dao;

import ${packagePath}.dal.po.${simpleClassName};
import com.${corpName}.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ${simpleClassName}Mapper extends BaseMapper<${simpleClassName}>{


}