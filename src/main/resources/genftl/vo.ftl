package ${packagePath}.biz.vo;

import ${packagePath}.dal.po.${simpleClassName};
import lombok.Data;

@Data
public class ${simpleClassName}VO extends ${simpleClassName} {

}
