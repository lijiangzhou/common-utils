package ${packagePath}.web;

import com.${corpName}.common.bean.BaseResult;
import com.${corpName}.common.bean.PageUtilsEasyUI;
import ${packagePath}.biz.service.${simpleClassName}Service;
import ${packagePath}.biz.vo.${simpleClassName}VO;
import ${packagePath}.dal.po.${simpleClassName};
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("${urlPath}")
public class ${simpleClassName}Controller {
    @Resource
    private ${simpleClassName}Service ${propertyName}Service;

    @RequestMapping("/list")
    public String list(Model model) {
        return "";
    }

    @RequestMapping("/edit")
    public String edit(String pkId, Model model) {
        ${simpleClassName} entity = ${propertyName}Service.getPoByPkId(pkId);
        model.addAttribute("entity", null == entity ? new ${simpleClassName}() : entity);
        return "";
    }

    @ResponseBody
    @RequestMapping("/update")
    public BaseResult update(${simpleClassName}VO vo) {
       return ${propertyName}Service.update(vo);
    }

    @ResponseBody
    @RequestMapping("pageList")
    public PageUtilsEasyUI pageList(${simpleClassName}VO vo) {
        return ${propertyName}Service.pageVoList(vo);
    }
}
