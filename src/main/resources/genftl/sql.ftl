CREATE TABLE ${tableName}
(
    id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT '自增id',
    pk_id VARCHAR(48) COMMENT '唯一id',
    is_deleted tinyint(4) not null default 0 COMMENT  '是否删除 1:是，0：不是',
[#list fieldList as field]
  [#if field.fieldType=='String']
    ${field.fieldDb} VARCHAR(256) COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='Long']
   ${field.fieldDb} BIGINT COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='Integer']
    ${field.fieldDb} INT COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='Byte']
    ${field.fieldDb} tinyint(4) COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='Enum']
    ${field.fieldDb} tinyint(4) not null default 0 COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='Date']
    ${field.fieldDb} timestamp COMMENT '${field.fieldCommet!}',
  [/#if]
  [#if field.fieldType=='BigDecimal']
    ${field.fieldDb} decimal(10,2) COMMENT '${field.fieldCommet!}',
   [/#if]
 [/#list]
    created_at timestamp null COMMENT '创建时间',
    updated_at timestamp null COMMENT '更改时间'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
CREATE UNIQUE INDEX ${tableName}_id_uindex ON ${tableName} (id);
CREATE UNIQUE INDEX ${tableName}_pk_id_uindex ON ${tableName} (pk_id);

