package ${packagePath}.biz.service;

import ${packagePath}.biz.vo.${simpleClassName}VO;
import ${packagePath}.dal.po.${simpleClassName};
import ${packagePath}.dal.dao.${simpleClassName}Mapper;
import com.${corpName}.common.base.BaseService2;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Service
public class ${simpleClassName}Service extends BaseService2<${simpleClassName}VO,${simpleClassName}> {

    @Resource
    private ${simpleClassName}Mapper ${propertyName}Mapper;

    @PostConstruct
    public void init(){
        setBaseMapper(${propertyName}Mapper);
    }

}
