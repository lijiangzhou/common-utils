<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packagePath}.dal.dao.${simpleClassName}Mapper" >
  <resultMap id="BaseResultMap" type="${packagePath}.dal.po.${simpleClassName}" >
    <id column="id" property="id" />
    <result column="pk_id" property="pkId" />
  [#list fieldList as field]
   [#if field.fieldType=='Enum']
    <result column="${field.fieldDb}" property="${field.fieldName}" typeHandler="${packagePath}.dal.base.IntegerAbleEnumHandler"/>
   [#else]
    <result column="${field.fieldDb}" property="${field.fieldName}" />
   [/#if]
  [/#list]
    <result column="is_deleted" property="isDeleted" />
    <result column="updated_at" property="updatedAt" />
    <result column="created_at" property="createdAt" />
   </resultMap>

   <sql id="Select_Column" >
    pk_id[#list fieldList as field],${field.fieldDb}[/#list]
   </sql>

   <sql id="Where_Sql" >
         <if test="pkId != null and pkId !=''" >
              and pk_id = [#noparse]#{pkId}[/#noparse]
         </if>
      [#list fieldList as field]
        [#if field.fieldType=='String']
          <if test="${field.fieldName} != null and ${field.fieldName} != ''" >
            and [#noparse][/#noparse] ${field.fieldDb} [#noparse]=#{[/#noparse]${field.fieldName}[#noparse]}[/#noparse]
          </if>
        [#else]
          <if test="${field.fieldName} != null" >
            and [#noparse][/#noparse] ${field.fieldDb} [#noparse]=#{[/#noparse]${field.fieldName}[#noparse]}[/#noparse]
          </if>
        [/#if]
      [/#list]
      </sql>

    <insert id="save" parameterType="${packagePath}.dal.po.${simpleClassName}">
        insert into  ${tableName}(
           pk_id[#list fieldList as field],${field.fieldDb}[/#list],updated_at,created_at
        ) values ([#noparse]#{pkId}[/#noparse]
        [#list fieldList as field]
           [#noparse],#{[/#noparse]${field.fieldName}[#noparse]}[/#noparse]
        [/#list],
       [#noparse] #{createdAt},#{createdAt} [/#noparse]
        )
   </insert>

   <insert id="saveBatch" parameterType="java.util.List">
        insert into ${tableName}(pk_id[#list fieldList as field],${field.fieldDb}[/#list],
        updated_at, created_at) values
        <foreach item="po" collection="list" separator=",">
         ([#noparse]#{po.pkId}[/#noparse]
           [#list fieldList as field]
              [#noparse],#{po.[/#noparse]${field.fieldName}[#noparse]}[/#noparse]
           [/#list], [#noparse] #{po.createdAt},#{po.createdAt} [/#noparse]
         )
        </foreach>
    </insert>

  <select id="get" parameterType="java.lang.String"  resultMap="BaseResultMap">
    select <include refid="Select_Column" /> from ${tableName}
    where pk_id = [#noparse]#{pkId}[/#noparse]
  </select>

  <select id="list" parameterType="${packagePath}.dal.po.${simpleClassName}" resultMap="BaseResultMap" >
    select <include refid="Select_Column" /> from ${tableName} where 1 = 1
    <include refid="Where_Sql"></include>
  </select>

   <select id="count" parameterType="${packagePath}.dal.po.${simpleClassName}" resultType="java.lang.Integer">
      select count(1) from ${tableName} where 1 = 1
      <include refid="Where_Sql"></include>
   </select>

  <delete id="delete" parameterType="java.lang.String">
      delete from ${tableName}  where pk_id = [#noparse]#{pkId}[/#noparse]
  </delete>

  <update id="update" parameterType="${packagePath}.dal.po.${simpleClassName}">
    update ${tableName}
      <set >
       [#list fieldList as field]
        [#if field.fieldType=='String']
            <if test="${field.fieldName} != null and ${field.fieldName} != ''" >
               [#noparse][/#noparse] ${field.fieldDb} [#noparse]=#{[/#noparse]${field.fieldName}[#noparse]}[/#noparse],
            </if>
        [#else]
            <if test="${field.fieldName} != null" >
               [#noparse][/#noparse] ${field.fieldDb} [#noparse]=#{[/#noparse]${field.fieldName}[#noparse]}[/#noparse],
            </if>
       [/#if]
       [/#list]
        <if test="updatedAt != null" >
            updated_at = [#noparse]#{updatedAt}[/#noparse]
        </if>
    </set>
      where pk_id = [#noparse]#{pkId}[/#noparse]
  </update>
</mapper>