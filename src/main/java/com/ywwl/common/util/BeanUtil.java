package com.ywwl.common.util;

import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author ybf
 */
public class BeanUtil extends org.apache.commons.beanutils.BeanUtils {

    /**
     * 对象拷贝 数据对象空值不拷贝到目标对象
     * @throws NoSuchMethodException copy
     */
    public static void copyBeanNotNull2Bean(Object databean, Object tobean) {
        if(databean instanceof Map)
        {
            copyMap2Bean(tobean,(Map)databean);
        }
        else
        {
            PropertyDescriptor origDescriptors[] = PropertyUtils
                    .getPropertyDescriptors(databean);
            for (int i = 0; i < origDescriptors.length; i++) {
                String name = origDescriptors[i].getName();
                // String type = origDescriptors[i].getPropertyType().toString();
                if ("class".equals(name)) {
                    continue; // No point in trying to set an object's class
                }
                if (PropertyUtils.isReadable(databean, name)
                        && PropertyUtils.isWriteable(tobean, name)) {
                    try {
                        Object value = PropertyUtils.getSimpleProperty(databean,
                                name);
                        Object toType = PropertyUtils.getPropertyType(tobean, name);

                        if (value != null) {
                        	
                            if (value instanceof String) {
                                value = ((String) value).trim();
                                
                                try {
                                		Class clazz=Class.forName(toType.toString().substring(6, toType.toString().length()));
                                    if (clazz.isEnum()) {
    	                                		Enum enumInfo = Enum.valueOf(clazz, (String) value);
    	                                		value = enumInfo;
                                    }
								} catch (Exception e) {
									// TODO: handle exception
								}
                            } 
                            copyProperty(tobean, name, value);
                        }
                    } catch (IllegalArgumentException ie) {
                       throw new RuntimeException(ie);
                    } catch (Exception e) {
                        ; // Should not happen
                    }
                }
            }
        }
    }

    public static Map<String, Object> copyBean2Map(Map<String, Object> map, Object bean) {
        if (null == map) {
            map = new HashMap<String, Object>();
        }
        PropertyDescriptor[] pds = PropertyUtils.getPropertyDescriptors(bean);
        for (int i = 0; i < pds.length; i++) {
            PropertyDescriptor pd = pds[i];
            String propname = pd.getName();
            try {
                if ("class".equals(propname)) {
                    continue; // No point in trying to set an object's class
                }

                Object propvalue = PropertyUtils.getSimpleProperty(bean,
                        propname);
                if (null != propvalue) {
                    map.put(propname, propvalue);
                }

            } catch (IllegalAccessException e) {
                // e.printStackTrace();
            } catch (InvocationTargetException e) {
                // e.printStackTrace();
            } catch (NoSuchMethodException e) {
                // e.printStackTrace();
            }
        }

        return map;
    }

    /**
     * 将Map内的key与Bean中属性相同的内容复制到BEAN中
     *
     * @param bean       Object
     * @param properties Map
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void copyMap2Bean(Object bean, Map<String, Object> properties) {
        // Do nothing unless both arguments have been specified
        if ((bean == null) || (properties == null)) {
            return;
        }
        // Loop through the property name/value pairs to be set
        Iterator<Map.Entry<String, Object>> entrys = properties.entrySet().iterator();
        while (entrys.hasNext()) {
            Map.Entry<String, Object> entry = entrys.next();
            String name = entry.getKey();
            // Identify the property name and value(s) to be assigned
            if (name == null) {
                continue;
            }
            Object value = entry.getValue();
            try {
                Class<?> clazz = PropertyUtils.getPropertyType(bean, name);
                if (null == clazz) {
                    continue;
                }
                String className = clazz.getName();
                if (className.equalsIgnoreCase("java.sql.Timestamp")) {
                    if (value == null || value.equals("")) {
                        continue;
                    }
                }
                setProperty(bean, name, value);
            } catch (NoSuchMethodException e) {
                continue;
            }
            catch (Exception e) {
                continue;
            }
        }
    }

    /**
     * Map内的key与Bean中属性相同的内容复制到BEAN中 对于存在空值的取默认值
     *
     * @param bean         Object
     * @param properties   Map
     * @param defaultValue String
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void copyMap2Bean(Object bean, Map properties,
                                    String defaultValue){
        // Do nothing unless both arguments have been specified
        if ((bean == null) || (properties == null)) {
            return;
        }
        // Loop through the property name/value pairs to be set
        Iterator entrys = properties.entrySet().iterator();
        while (entrys.hasNext()) {
            Map.Entry entry = (Map.Entry) entrys.next();
            String name = (String) entry.getKey();
            // Identify the property name and value(s) to be assigned
            if (name == null) {
                continue;
            }
            Object value = entry.getValue();
            try {
                Class clazz = PropertyUtils.getPropertyType(bean, name);
                if (null == clazz) {
                    continue;
                }
                String className = clazz.getName();
                if (className.equalsIgnoreCase("java.sql.Timestamp")) {
                    if (value == null || value.equals("")) {
                        continue;
                    }
                }
                if (className.equalsIgnoreCase("java.lang.String")) {
                    if (value == null) {
                        value = defaultValue;
                    }
                }
                setProperty(bean, name, value);
            } catch (NoSuchMethodException e) {
                continue;
            }
            catch (Exception e) {
                continue;
            }
        }
    }
}


