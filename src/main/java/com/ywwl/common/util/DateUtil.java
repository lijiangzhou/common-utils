package com.ywwl.common.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

import com.ywwl.common.constant.DateFormatPattern;
import com.ywwl.common.exception.BizException;


public class DateUtil {

    public static Long getDateLongTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatPattern.YYYY_MM_DD);
        String dateString = sdf.format(date);
        if (StringUtils.isNumeric(dateString)) {
            return Long.parseLong(dateString);
        }
        return null;
    }

    /**
     * 时间戳按指定格式转换字符串
     * 
     * @param stamp 时间戳
     * @param pattern 
     * @return
     * @Date 2018年11月7日下午6:40:27
     */
    public static String format(Long stamp, String pattern) {
    		Date date = new Date(stamp);
    		return format(date, pattern);
    }
   
    public static String format(Date d) {
        return format(d, null);
    }

    public static String format(Date d, String pattern) {
        return format(d, pattern, null);
    }

    public static String format(Date d, String pattern, TimeZone timeZone) {
        if (d == null) {
            return "";
        }
        pattern = StringUtils.isBlank(pattern) ? "yyyy-MM-dd HH:mm:ss.SSSZ" : pattern;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        if (timeZone != null) {
            format.setTimeZone(timeZone);
        }
        return format.format(d);
    }

    public static Date parse(String str) {
        return parse(str, (String) null);
    }

    public static Date parse(String source, String pattern) {
        try {
            return parse(source, pattern, (TimeZone) null);
        } catch (ParseException e) {
            throw new BizException(String.format("格式化错误，source: %s, pattern: %s", source, pattern));
        }
    }

    public static String formatUnixStamp(Long unixStamp, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(unixStamp);
    }
    
    public static Date parse(String source, String pattern, TimeZone timeZone) throws ParseException {
        if (source == null) {
            return null;
        }
        pattern = StringUtils.isBlank(pattern) ? "yyyy-MM-dd HH:mm:ss.SSSZ" : pattern;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        if (timeZone != null) {
            format.setTimeZone(timeZone);
        }
        return format.parse(source);

    }

    /**
     * localDateTime 转 Date
     *
     * @param localDateTime
     * @return
     * @author 陈泰（周利江）
     * @Date 2018年7月31日上午11:41:24
     */
    public static Date getDate(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDateTime.atZone(zoneId);
        return Date.from(zdt.toInstant());
    }
    
    /**
     * Date 转 LocalDate
     * 
     * @param date
     * @return
     */
    public static LocalDate getLocalDate(Date date) {
    	if (date == null) {
    		return null;
    	}
    	return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
    /**
     * Date 转 LocalDateTime
     * 
     * @param date
     * @return
     */
    public static LocalDateTime getLocalDateTime(Date date) {
    	if (date == null) {
    		return null;
    	}
    	return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    
    /**
     * LocalDate 转 Date
     * 
     * @param localDate
     * @return
     */
    public static Date getDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        ZonedDateTime zdt = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    /**
     * 计算两个时间相差多少
     *
     * @param nd 要计算的时间的单位 如：小时：nh=60*60*1000,分钟：nm=1*60*1000
     */
    public static double getDatePoor(Date startDate, Date endDate, double nd) {
        return (endDate.getTime() - startDate.getTime()) / nd;
    }

    /**
     * 获取传入日期N天后的时间，负数表示N天前
     *
     * @param date
     * @param days
     * @return
     */
    public static Date getAfterDay(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }
    
    public static Date getBeforeDay(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -days);
        return calendar.getTime();
    }
    
    public static void main(String[] args) {
		Date date = new Date();
		System.out.println(DateUtil.getLocalDate(date).toString());
		System.out.println(DateUtil.getLocalDateTime(date).toString());
	}
    

}
