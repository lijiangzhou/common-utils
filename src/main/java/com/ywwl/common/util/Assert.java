/**
 * File generated at: 2017年11月9日上午10:47:33
 */
package com.ywwl.common.util;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.ywwl.common.exception.BizException;

/**
 * 断言 
 * 
 * @author 陈泰（周利江）
 * @Date 2017年11月9日上午10:47:33
 *
 */
public  class Assert {

    private Assert() {
    }
    
    public static void notGreaterThan(int org, int to) {
    		if (org > to) {
    		    throw new BizException(-1,String.format("%s  不能大于 %s", org, to));
            }
    }
    
    public static void notGreaterThan(int org, int to, String msg) {
		if (org > to) {
		    throw new BizException(-1,String.format("%s  %s  不能大于 %s",msg, org, to));
        }
}
    
    public static void notNull(Object obj) {
        if (obj == null) {
            throw new BizException(-1, "null");
        }
        if (obj instanceof String) {
        		if (StringUtils.isEmpty(obj.toString())) {
        			throw new BizException(String.format("对象 不存在"));
        		}
        }
    }

    public static void notNull(Object obj, String message) {
        if (obj == null) {
            throw new BizException(String.format("%s 不存在", message));
        }
    }
    
    public static void notBlank(String obj) {
    		notBlank(obj, "");
    }
    
    public static void notBlank(String obj, String message) {
        if (StringUtils.isBlank(obj)) {
            throw new BizException(String.format("%s 不能为空", message));
        }
    }
    
    public static void collectionNotEmpty(Collection<?> collection, String message) {
        if (CollectionUtils.isEmpty(collection)) {
            throw new BizException(String.format("%s cannot empty", message));
        }
    }
    
    public static void error(String message) {
        throw new BizException(-1,message);
    }

}
