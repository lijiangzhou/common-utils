/**
 * File generated at: 2017年11月23日下午9:48:04
 */
package com.ywwl.common.constant;

/**
 * 错误信息接口 
 * 
 * @author 陈泰（周利江）
 * @Date 2017年11月23日下午9:48:04
 *
 */
public interface IErrorCode {

	/**
	 * 错误码
	 * 
	 * @return
	 * @author 陈泰（周利江）
	 * @Date 2017年11月23日下午9:49:50
	 */
	public Integer getErrorCode();
	
	/**
	 * 错误信息
	 * 
	 * @return
	 * @author 陈泰（周利江）
	 * @Date 2017年11月23日下午9:49:58
	 */
	public String getErrorMsg();
}
