/**
 * File generated at: 2017年11月8日上午11:47:30
 */
package com.ywwl.common.constant;

/**
 * 日期常量类
 * 
 * @author 陈泰（周利江）
 * @Date 2017年11月8日上午11:47:30
 *
 */
public final class DateFormatPattern {

	public final static String YYYY_MM_DD = "yyyy-MM-dd";
	
	public final static String YYYYMMDD = "yyyyMMdd";
	
	public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	
	public final static String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
	
	public final static String YYYY_MM_CN = "yyyy年MM月";
	
	public final static String MM_DD_CN = "MM月dd日";
	
	public final static String HH_MM_SS = "HH:mm:ss";
	
	public final static String MM = "mm";
}
