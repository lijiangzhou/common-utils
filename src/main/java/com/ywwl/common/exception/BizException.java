package com.ywwl.common.exception;

import com.ywwl.common.constant.IErrorCode;

public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;

	public BizException(int code, String message, Throwable cause) {
		super(message, cause);
		errorCode = code;
	}

	public BizException(String message) {
		this(1, message, null);
	}
	
	public BizException(IErrorCode errorCode) {
		this(errorCode.getErrorCode(), errorCode.getErrorMsg(), null);
	}
	
	public BizException(IErrorCode errorCode, Throwable cause) {
		this(errorCode.getErrorCode(), errorCode.getErrorMsg(), cause);
	}

	public BizException(int code, String message) {
		this(code, message, null);
	}

	public BizException(int code, Throwable cause) {
		this(code, null, cause);
	}

	public int getErrorCode() {
		return errorCode;
	}
}
