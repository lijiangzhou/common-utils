/**
 * File generated at: 2018年11月5日上午10:59:34
 */
package com.ywwl.common.bean;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * 对Page<E>结果进行包装, 对com.github.pagehelper.PageInfo做适度更改
 * <p/>
 * 新增分页的多项属性，主要参考:http://bbs.csdn.net/topics/360010907
 *
 * @author liuzh/abel533/isea533
 * @version 3.3.0
 * @since 3.2.2
 * 项目地址 : http://git.oschina.net/free/Mybatis_PageHelper
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class PageUtils<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    //每页的数量
    private int pageSize = 15;
    //当前页的数量
    private int size;
    //总记录数
    private long total;
    //总页数
    private int pages;
    //结果集
    private List<T> rows;
    //其他结果集
    private List<T> otherRows;
    // 其他结果集大小
    private int otherSize;

    public PageUtils() {
    }

    public PageUtils(int pageSize, int total, List<T> rows, List<T> otherRows) {
		this.pageSize = pageSize;
	    this.total = total;
	    this.rows = rows;
	    this.otherRows = otherRows;
	    this.size = rows.size();
	    if (CollectionUtils.isNotEmpty(otherRows)) {
	    	this.otherSize = otherRows.size();
	    } else {
	    	this.otherSize = 0;
	    }
    }
    
    public PageUtils(int pageSize, int total, List<T> rows) {
		this.pageSize = pageSize;
	    this.total = total;
	    this.rows = rows;
	    this.size = rows.size();
    }
    
    public PageUtils(List<T> rows, int pageSize, long total) {
    		this.pageSize = pageSize;
        this.total = total;
        this.rows = rows;
        this.size = rows.size();
    }
    
    public PageUtils(List<T> rows, long total) {
		this.total = total;
	    this.rows = rows;
	    this.size = rows.size();
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PageInfo{");
        sb.append(", size=").append(size);
        sb.append(", total=").append(total);
        sb.append(", pages=").append(pages);
        sb.append(", rows=").append(rows);
		sb.append("}");
        return sb.toString();
    }


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public List<T> getOtherRows() {
		return otherRows;
	}


	public void setOtherRows(List<T> otherRows) {
		this.otherRows = otherRows;
	    if (CollectionUtils.isNotEmpty(otherRows)) {
	    	this.otherSize = otherRows.size();
	    } else {
	    	this.otherSize = 0;
	    }
	}

	public int getOtherSize() {
		return otherSize;
	}

	public void setOtherSize(int otherSize) {
		this.otherSize = otherSize;
	}
}

