package com.ywwl.common.bean;


import java.io.Serializable;

public class Pager implements Serializable {
    private static final long serialVersionUID = 5883636778638787974L;
    private int totalPage = 1; // 总页数
    private int totalNum = 0; // 总记录数
    private int pageSize = 15; // 每页行数
    private int pageIndex = 1; // 当前页码,-1时不查询分页
    private boolean isCount = true;//是否要查询总数
    private int startIndex;
    private String sidx;//排序字段
    private String sord;//排序方式(asc、desc)

    public Pager() {

    }

    public Pager(int page, int rows) {
        this.pageIndex = page;
        this.pageSize = rows;
    }

    public Pager(int page, int rows, String sidx, String sord) {
        this.pageIndex = page;
        this.pageSize = rows;
        this.sidx = sidx;
        this.sord = sord;
    }

    public int getStartIndex() {
        if (getPageIndex() <= 0) {
            return 0;
        }
        return getPageSize() * (getPageIndex() - 1);
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        if (pageIndex == 0) {
            this.pageIndex = 1;
        } else {
            this.pageIndex = pageIndex;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        if (totalNum != 0 || pageSize != 0) {
            totalPage = totalNum / pageSize;
            if (totalNum % pageSize != 0) {
                totalPage++;
            }
        }
        if (totalPage == 0) {
            totalPage = 1;
        }
        return totalPage;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }
}
