package com.ywwl.common.bean;


import java.io.Serializable;

import com.ywwl.common.constant.IErrorCode;


/**
 */
public class BaseResult<T> implements Serializable {

	private static final long serialVersionUID = 4998958908867934913L;

    /**
     * 信息反馈
     */
    public static final String SUCCESS_MSG = "操作成功!";
    public static final String FAILED_MSG = "操作失败!";
    protected int code;
    protected String msg;
    protected T data;
    protected Boolean success;
    
    /**
	 * @return success
	 */
	public Boolean isSuccess() {
		return success;
	}

	/**
	 * @param other the other to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public BaseResult() {
        this.success = true;
        this.msg = SUCCESS_MSG;
    }

    public BaseResult(T data) {
        this.msg = SUCCESS_MSG;
        this.success = true;
        this.data = data;
    }
    
    public BaseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
       this.success = false;
    }
    
    public BaseResult(IErrorCode code) {
        this.code = code.getErrorCode();
        this.msg = code.getErrorMsg();
        this.success = false;
    }

    public BaseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.success = false;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
